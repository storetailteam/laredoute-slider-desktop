"use strict";

try {

  const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 2,
    ctxt_bg = 4,
    ctxt_logo = 5;

  var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    style = require("./main.css"),
    container = $(html),
    creaid = settings.creaid,
    format = settings.format,
    custom = settings.custom,
    products = settings.products,
    target = settings.cta_target != "_blank" && settings.target != "_self" ? "_self" : settings.target,
    container = $(html),
    helper_methods = sto.utils.retailerMethod,
    sto_global_products = [],
    sto_global_nbproducts = 0,
    pos, posGlobals;


  var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);


  module.exports = {
    init: _init_()
  }

  function _init_() {

    sto.load([format, creaid], function (tracker) {

      var removers = {};
      removers[format] = {
        "creaid": creaid,
        "func": function () {
          style.unuse();
          container.remove(); //verifier containerFormat j'ai fait un copier coller
        }
      };


      if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 3) {
        return removers;
      }

      /*if ($('div[data-pos="' + settings.btfpos + '"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }*/

      // <editor-fold> CARROUSEL PRODUITS ***************************************************
      if (settings.crawl != "" && settings.products.length > 0 && settings.custom.length < 1) {
        helper_methods.crawl(settings.crawl).promise.then(function (d) {

          var prodCounter = 0;
          for (var f = 0; f < settings.products.length; f++) {
            if (d[settings.products[f]]) {
              prodCounter++;
            }
          }

          if (prodCounter < 3) {
            tracker.error({
              "tl": "lessThanThreeProd"
            });
            return removers;
            return false;
          }

          try {
            // <editor-fold> INSERT CONTAINER *****************************************************
            // if ($('div[data-pos="13"]').length <= 0 && $('div[data-pos="25"]').length <= 0) {
            //   container.attr('data-pos', '13');
            // } else if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length <= 0) {
            //   container.attr('data-pos', '25');
            // }

            var prodsInpage = $('#productList>li').length;

            if (prodsInpage >= 14 && prodsInpage <= 26 && $('.sto-' + format).length < 1) {
              helper_methods.createFormat(container, tracker, settings.products, d);
            } else if (prodsInpage > 26 && $('.sto-' + format).length < 3) {
              helper_methods.createFormat(container, tracker, settings.products, d);
            } else {
              tracker.error({
                "tl": "notEnoughProdsInPage"
              });
              return removers;
              return false;
            }

            //insert format
            var insertFormat = function () {

              var widthTile = $('#productList>li').width();
              var widthLine = $('.product-list').width();
              var nbTile;

              if (widthTile * 2 > widthLine) {
                nbTile = 1;
              } else if (widthTile * 3 > widthLine) {
                nbTile = 2;
              } else {
                nbTile = 3;
              }

              var sponso = $('.hl-beacon-universal').length;
              if (sponso >= 1) {
                sponso = 1;
              }

              var sponsoTech = $('.breakzone').length;
              if (sponsoTech >= 1) {
                sponsoTech = 1;
              }
              // if ($('.sto-format').length == 0 && sponsoTech >= 1 && nbTile == 1) {
              //   sponsoTech = 5;
              // } else if ($('.sto-format').length == 1 && sponsoTech >= 1 && nbTile == 1) {
              //   sponsoTech = 1;
              // } else if ($('.sto-format').length == 2 && sponsoTech >= 1 && nbTile == 1) {
              //   sponsoTech = -3;
              // } else {
              //   sponsoTech = 0;
              // }

              if (window.matchMedia("(max-width: 767px)").matches && nbTile == 1) {
                if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                  $('#productList>li:visible').eq(nbTile * 5).before(container);
                  container.attr('data-pos', 6);
                  posGlobals = 6;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                  $('#productList>li:visible').eq(nbTile * 14).before(container);
                  container.attr('data-pos', 16);
                  posGlobals = 16;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                  $('#productList>li:visible').eq(nbTile * 23).before(container);
                  container.attr('data-pos', 26);
                  posGlobals = 26
                }
              } else if (window.matchMedia("(min-width: 768px)").matches && nbTile == 1) {
                if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                  $('#productList>li:visible').eq(nbTile * (11 + sponsoTech)).before(container);
                  container.attr('data-pos', 12 + sponsoTech);
                  posGlobals = 12;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                  $('#productList>li:visible').eq(nbTile * (16 + sponsoTech)).before(container);
                  container.attr('data-pos', 18 + sponsoTech);
                  posGlobals = 18;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                  $('#productList>li:visible').eq(nbTile * (21 + sponsoTech)).before(container);
                  container.attr('data-pos', 24 + sponsoTech);
                  posGlobals = 24
                }
              } else {
                if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                  $('#productList>li:visible').eq(nbTile * (5 + sponso)).before(container);
                  container.attr('data-pos', 6 + sponso);
                  posGlobals = 6;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                  $('#productList>li:visible').eq(nbTile * (14 + sponso)).before(container);
                  container.attr('data-pos', 16 + sponso);
                  posGlobals = 16;
                } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                  $('#productList>li:visible').eq(nbTile * (23 + sponso)).before(container);
                  container.attr('data-pos', 26 + sponso);
                  posGlobals = 26
                }
              }
            }

            insertFormat();

            pos = $('.sto-' + placeholder + '-w-carrousel').attr('data-pos');

            // </editor-fold> *********************************************************************

            // <editor-fold> AVAILABILITY ****************************************************************************
            function verifAvailability(a, d) {
              for (var i = 0; i <= a.length; i++) {
                var value = a[i];
                if (d[value] !== undefined && d[value].dispo !== false) {
                  sto_global_products.push(value);
                  sto_global_nbproducts += 1;
                }
              }
              return sto_global_nbproducts;
            }

            var available = verifAvailability(products, d);

            if (available = !0) {
              tracker.availability({
                "po": pos,
                "tn": sto_global_nbproducts
              });
            }
            // </editor-fold> ****************************************************************************************

            style.use();

            tracker.display({
              "po": pos
            });

            var trackingView = false;
            var windowCenterTop = $(window).height() / 4;
            var windowCenterBot = $(window).height() - windowCenterTop;
            var eTop = $('.sto-' + placeholder + '-w-carrousel').offset().top;

            var dataPosition = $('.sto-' + placeholder + '-w-carrousel').attr('data-pos');


            var distanceToTop;
            var trackScroll = function () {
              distanceToTop = eTop - $(window).scrollTop();
              if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                tracker.view({
                  "po": pos
                });
                trackingView = true;
              }
            };
            trackScroll();


            // $(window).scroll(function() {
            //   trackScroll();
            // });

            //$('.sto-' + placeholder + '-w-carrousel').attr('data-pos', settings.btfpos);
            var lastProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-product-klass').eq($('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-product-klass').length - 1).detach();

            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').prepend(lastProd);

            $('.sto-' + placeholder + '-w-carrousel').attr('data-type', 'products');
            $('.sto-' + placeholder + '-w-carrousel').attr('data-format-type', settings.format);
            $('.sto-' + placeholder + '-w-carrousel').attr('data-crea-id', settings.creaid);


            var slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-product-klass').length,
              pageW, slidesQty, sliderW;


            // tracker click
            $('.sto-' + placeholder + '-w-slider .sto-product-container .sto-product-klass').on('click', function () {
              var product = $(this).attr('data-productid');
              tracker.click({
                "tl": product,
                "po": pos
              });
            });


            window.addEventListener("resize", function () {

              var resizeFormat = function () {
                var widthTile = $('#productList>li').width();
                var widthLine = $('.product-list').width();
                var nbTile;

                if (widthTile * 2 > widthLine) {
                  nbTile = 1;
                } else if (widthTile * 3 > widthLine) {
                  nbTile = 2;
                } else {
                  nbTile = 3;
                }

                $('.sto-format').each(function () {
                  var dataPos = $(this).attr('data-pos');

                  // if (window.matchMedia("(max-width: 767px)").matches) {

                  if (nbTile == 1) {
                    if (window.matchMedia("(max-width: 767px)").matches) {
                      if (dataPos >= 6 && dataPos <= 13) {
                        $('#productList>li:visible').eq(nbTile * 5).before(this);
                      } else if (dataPos >= 16 && dataPos <= 20) {
                        $('#productList>li:visible').eq(nbTile * 14).before(this);
                      } else if (dataPos >= 22 && dataPos <= 28) {
                        $('#productList>li:visible').eq(nbTile * 23).before(this);
                      }
                    }
                    if (window.matchMedia("(min-width: 768px)").matches) {
                      if (dataPos >= 6 && dataPos <= 13) {
                        $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
                      } else if (dataPos >= 16 && dataPos <= 20) {
                        $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
                      } else if (dataPos >= 22 && dataPos <= 28) {
                        $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
                      }
                    }
                  } else if (nbTile > 1) {
                    if (dataPos >= 6 && dataPos <= 13) {
                      $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
                    } else if (dataPos >= 16 && dataPos <= 17) {
                      $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
                    } else if (dataPos >= 23 && dataPos <= 27) {
                      $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
                    }
                  }
                });
              }
              resizeFormat();

              var size = $(window).width();

              pageW = $('#productList').outerWidth() - 15;
              //slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length;
              slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-product-klass').length;
              //$('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').width((pageW / 2) - 14);


              $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-product-klass').width((pageW / 2) - 14);
              //sliderW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').outerWidth();
              sliderW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-product-klass').outerWidth();

              $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width(sliderW * slidesQty);


              //Format height homotetique
              if (size < 400) {
                $('.sto-' + placeholder + '-w-carrousel').css('height', ((90 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
              } else {
                $('.sto-' + placeholder + '-w-carrousel').css('height', ((80 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
              }

              $('.sto-' + placeholder + '-w-carrousel .sto-product-klass').css('width', ((40 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
              $('.sto-' + placeholder + '-w-carrousel .pl-image').css('height', $('.sto-' + placeholder + '-product-img').width());

              $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('width', $('.sto-' + placeholder + '-w-carrousel .sto-product-klass').outerWidth(true) * slidesQty + 10 + 'px');

              //Logo height homotetique
              $('.sto-' + placeholder + '-logo').css('height', ((25 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');


              // slider left margin
              if (window.matchMedia("(max-width: 767px)").matches) {
                $('.sto-' + placeholder + '-slider.sto-product-container').css({
                  'left': '15px',
                  'transform': 'translateX(0%)'
                });
              } else {
                if ($('.sto-' + placeholder + '-slider').width() <= $('.sto-' + placeholder + '-w-slider').width()) {
                  $('.sto-' + placeholder + '-slider-right').fadeOut(1);
                  $('.sto-' + placeholder + '-slider').css({
                    'left': 'calc(50% + 8px)',
                    'transform': 'translateX(-50%)'
                  })
                } else {
                  $('.sto-' + placeholder + '-slider-right').fadeIn(1);
                  $('.sto-' + placeholder + '-slider').css({
                    'left': '50px',
                    'transform': 'translateX(0%)'
                  })
                }
              }

            });


            // <editor-fold> CARROUSEL ARROWS *****************************************************
            if ($('.sto-' + placeholder + '-slider').width() <= $('.sto-' + placeholder + '-w-slider').width()) {
              $('.sto-' + placeholder + '-slider-right').fadeOut(1);
              $('.sto-' + placeholder + '-slider').css({
                'left': 'calc(50% + 8px)',
                'transform': 'translateX(-50%)'
              })
            }

            var slideDist = 400;
            $('.sto-' + placeholder + '-slider-left').fadeOut(10);


            // left
            $('.sto-' + placeholder + '-slider-left').on('click', function () {
              $('.sto-' + placeholder + '-slider-right').fadeIn(300);
              var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
              var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
              if ((positionInit * -1) < slideDist && positionInit < 50) {
                $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                  left: '50'
                }, 300, 'swing');
                $('.sto-' + placeholder + '-slider-left').fadeOut(300);
              } else if (positionInit < 50) {
                var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left + slideDist;
                $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                  left: positionLeft + 'px'
                }, 300, 'swing');
              }
              tracker.browse({
                "tl": "swipeLeft",
                "po": pos
              });
            });

            // right
            $('.sto-' + placeholder + '-slider-right').on('click', function () {
              $('.sto-' + placeholder + '-slider-left').fadeIn(300);
              var sliderContainerWidth = $('.sto-' + placeholder + '-w-slider').width();
              var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
              var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
              var positionRight = slideWidth - (sliderContainerWidth - positionInit);
              if (positionRight > slideDist) {
                var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left - slideDist;
                $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                  left: positionLeft + 'px'
                }, 300, 'swing');
              } else if (positionRight <= slideDist) {
                $('.sto-' + placeholder + '-slider.sto-product-container').animate({
                  left: (sliderContainerWidth - slideWidth - 35) + 'px'
                }, 300, 'swing');
                $('.sto-' + placeholder + '-slider-right').fadeOut(300);
              }
              tracker.browse({
                "tl": "swipeRight",
                "po": pos
              });
            });


            // tracking swipe
            var lastPos = 0;
            $('.sto-' + placeholder + '-w-slider').on('scroll', function () {
              var currPos = $('.sto-' + placeholder + '-w-slider').scrollLeft();
              clearTimeout($.data(this, 'scrollTimer'));
              $.data(this, 'scrollTimer', setTimeout(function () {

                if (currPos > lastPos) {
                  tracker.browse({
                    "tl": "swipeRight",
                    "po": pos
                  });
                }
                if (currPos < lastPos) {
                  tracker.browse({
                    "tl": "swipeLeft",
                    "po": pos
                  });
                }
                lastPos = currPos;
              }, 250));
            });

            // </editor-fold> *********************************************************************


            window.setTimeout(function () {
              //window.dispatchEvent(new window.Event("resize"));
              var evt = window.document.createEvent('UIEvents');
              evt.initUIEvent('resize', true, false, window, 0);
              window.dispatchEvent(evt);
            });

            //STORE FORMAT FOR FILTERS
            sto.globals["print_creatives"].push({
              html: $('.sto-' + placeholder + '-w-carrousel'),
              parent_container_id: "#productList",
              type: settings.format,
              crea_id: settings.creaid,
              position: posGlobals
            });

          } catch (e) {
            style.unuse();
            $(".sto-" + placeholder + "-container").remove();
            tracker.error({
              // "te": "onBuild-format",
              "tl": e
            });
          }
        }).then(null, console.log.bind(console));

      }
      // </editor-fold> *********************************************************************
      else

      // <editor-fold> CARROUSEL PUB ********************************************************
      {
        if (settings.custom.length < 3) {
          tracker.error({
            "tl": "lessThanTwoProds"
          });
          return removers;
          return false;
        }

        try {


          var prodsInpage = $('#productList>li').length;


          if (prodsInpage >= 14 && prodsInpage <= 26 && $('.sto-' + format).length < 1) {

          } else if (prodsInpage > 26 && $('.sto-' + format).length < 3) {

          } else {
            tracker.error({
              "tl": "notEnoughProdsInPage"
            });
            return removers;
            return false;
          }

          style.use();



          // <editor-fold> INSERT CONTAINER *****************************************************
          //insert format
          var insertFormat = function () {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            var sponso = $('.hl-beacon-universal').length;
            if (sponso >= 1) {
              sponso = 1;
            }

            var sponsoTech = $('.breakzone').length;
            if (sponsoTech >= 1) {
              sponsoTech = 1;
            }


            if (window.matchMedia("(max-width: 767px)").matches && nbTile == 1) {
              if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                $('#productList>li:visible').eq(nbTile * 5).before(container);
                container.attr('data-pos', 6);
                posGlobals = 6;
              } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                $('#productList>li:visible').eq(nbTile * 14).before(container);
                container.attr('data-pos', 16);
                posGlobals = 16;
              } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                $('#productList>li:visible').eq(nbTile * 23).before(container);
                container.attr('data-pos', 26);
                posGlobals = 26
              }
            } else if (window.matchMedia("(min-width: 768px)").matches && nbTile == 1) {
              if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                $('#productList>li:visible').eq(nbTile * (11 + sponsoTech)).before(container);
                container.attr('data-pos', 12 + sponsoTech);
                posGlobals = 12;
              } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                $('#productList>li:visible').eq(nbTile * (16 + sponsoTech)).before(container);
                container.attr('data-pos', 18 + sponsoTech);
                posGlobals = 18;
              } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                $('#productList>li:visible').eq(nbTile * (21 + sponsoTech)).before(container);
                container.attr('data-pos', 24 + sponsoTech);
                posGlobals = 24
              }
            } else {
              if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
                $('#productList>li:visible').eq(nbTile * (5 + sponso)).before(container);
                container.attr('data-pos', 6 + sponso);
                posGlobals = 6;
              } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
                $('#productList>li:visible').eq(nbTile * (14 + sponso)).before(container);
                container.attr('data-pos', 16 + sponso);
                posGlobals = 16;
              } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
                $('#productList>li:visible').eq(nbTile * (23 + sponso)).before(container);
                container.attr('data-pos', 26 + sponso);
                posGlobals = 26
              }
            }
          }

          insertFormat();

          pos = $('.sto-' + placeholder + '-w-carrousel').attr('data-pos');


          // </editor-fold> *********************************************************************


          tracker.display({
            "po": pos
          });


          $('.sto-' + placeholder + '-w-carrousel').attr('data-type', 'redirection');
          // $('.sto-' + placeholder + '-w-carrousel').attr('data-pos', settings.btfpos);
          $('.sto-' + placeholder + '-w-carrousel').attr('data-format-type', settings.format);
          $('.sto-' + placeholder + '-w-carrousel').attr('data-crea-id', settings.creaid);


          var trackingView = false;
          var windowCenterTop = $(window).height() / 4;
          var windowCenterBot = $(window).height() - windowCenterTop;
          var eTop = $('.sto-' + placeholder + '-w-carrousel').offset().top;

          var dataPosition = $('.sto-' + placeholder + '-w-carrousel').attr('data-pos');

          var distanceToTop;
          var trackScroll = function () {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
              tracker.view({
                // "tl": dataPosition
                "po": pos
              });
              trackingView = true;
            }
          };
          trackScroll();


          $(window).scroll(function () {
            trackScroll();
          });


          // <editor-fold> INSERT PRODUITS ******************************************************
          for (var i = 0; i < settings.custom.length; i++) {
            var prodInd = parseInt(settings.custom[i][0]);
            var prodClaimTxt = settings.custom[i][1];
            var prodCtaUrl = settings.custom[i][2];
            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').append('<li data-index="' + (prodInd + 1) + '" class="product sto-vignette-klass"><div class="sto-' + placeholder + '-product-img"></div><div class="sto-' + placeholder + '-w-cta"><div class="sto-' + placeholder + '-product-claim"><span>' + prodClaimTxt + '</span></div></div><div data-href="' + prodCtaUrl + '" class="sto-' + placeholder + '-product-cta"><span>' + settings.cta_text + '</span></div></li>');
          }

          // largeur carousel resize
          var carWidth = function () {
            var nbProduits = settings.custom.length;
            var widthProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>.sto-vignette-klass').width();
          }

          carWidth();

          // </editor-fold> *********************************************************************


          // <editor-fold> CLICK REDIRECTION ****************************************************
          $(document).on('click', '.sto-' + placeholder + '-w-slider .sto-product-container .product', function () {
            tracker.click({
              "tl": 'prod' + (parseInt($(this).attr('data-index') - 1)) + '_' + $(this).find('.sto-' + placeholder + '-product-claim span').text().replace(/ /g, ''),
              "po": pos
            });
            window.open($(this).find('.sto-' + placeholder + '-product-cta').attr('data-href'), target);
          });
          // </editor-fold> *********************************************************************

          var slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-vignette-klass').length,
            pageW, slidesQty, sliderW;
          window.addEventListener("resize", function () {

            var resizeFormat = function () {
              var widthTile = $('#productList>li').width();
              var widthLine = $('.product-list').width();
              var nbTile;

              if (widthTile * 2 > widthLine) {
                nbTile = 1;
              } else if (widthTile * 3 > widthLine) {
                nbTile = 2;
              } else {
                nbTile = 3;
              }

              $('.sto-format').each(function () {
                var dataPos = $(this).attr('data-pos');

                // if (window.matchMedia("(max-width: 767px)").matches) {

                if (nbTile == 1) {
                  if (window.matchMedia("(max-width: 767px)").matches) {
                    if (dataPos >= 6 && dataPos <= 13) {
                      $('#productList>li:visible').eq(nbTile * 5).before(this);
                    } else if (dataPos >= 16 && dataPos <= 20) {
                      $('#productList>li:visible').eq(nbTile * 14).before(this);
                    } else if (dataPos >= 22 && dataPos <= 28) {
                      $('#productList>li:visible').eq(nbTile * 23).before(this);
                    }
                  }
                  if (window.matchMedia("(min-width: 768px)").matches) {
                    if (dataPos >= 6 && dataPos <= 13) {
                      $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
                    } else if (dataPos >= 16 && dataPos <= 20) {
                      $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
                    } else if (dataPos >= 22 && dataPos <= 28) {
                      $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
                    }
                  }
                } else if (nbTile > 1) {
                  if (dataPos >= 6 && dataPos <= 13) {
                    $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
                  } else if (dataPos >= 16 && dataPos <= 17) {
                    $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
                  } else if (dataPos >= 23 && dataPos <= 27) {
                    $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
                  }
                }
              });
            }
            resizeFormat();


            sliderW = 0;
            for (var j = 0; j < slidesQty; j++) {
              sliderW = sliderW + $('.sto-' + placeholder + '-w-carrousel .sto-product-container> .sto-vignette-klass').eq(j).outerWidth(true, true);
            }

            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').outerWidth(sliderW + 2);

            //Format height homotetique
            $('.sto-' + placeholder + '-w-carrousel').css('height', ((65 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');

            $('.sto-' + placeholder + '-w-carrousel .product').css('width', ((40 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
            $('.sto-' + placeholder + '-product-img').css('height', $('.sto-' + placeholder + '-product-img').width());
            //Logo height homotetique
            $('.sto-' + placeholder + '-logo').css('width', ((33 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
            // $('.sto-' + placeholder + '-logo').css('height', ((75 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');

            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('width', $('.sto-' + placeholder + '-w-carrousel .product').outerWidth(true) * slidesQty + 'px');


            // displostion fleches
            var heightImg = $('.sto-' + placeholder + '-product-img').css('height');
            $('.sto-' + placeholder + '-slider-left, .sto-' + placeholder + '-slider-right').css('height', heightImg);


            // slider left margin
            if (window.matchMedia("(max-width: 767px)").matches) {
              $('.sto-' + placeholder + '-slider.sto-product-container').css({
                'left': '15px',
                'transform': 'translateX(0%)'
              });
            } else {
              if ($('.sto-' + placeholder + '-slider').width() <= $('.sto-' + placeholder + '-w-slider').width()) {
                $('.sto-' + placeholder + '-slider-right').fadeOut(1);
                $('.sto-' + placeholder + '-slider').css({
                  'left': 'calc(50% + 8px)',
                  'transform': 'translateX(-50%)'
                })
              } else {
                $('.sto-' + placeholder + '-slider-right').fadeIn(1);
                $('.sto-' + placeholder + '-slider').css({
                  'left': '50px',
                  'transform': 'translateX(0%)'
                })
              }
            }

            // $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
            //   'left': 15
            // }, 0);


          });
          window.setTimeout(function () {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
          });


          // STORE FORMAT FOR FILTERS
          sto.globals["print_creatives"].push({
            html: container,
            parent_container_id: "#productList",
            type: settings.format,
            crea_id: settings.creaid,
            position: posGlobals
          });

        } catch (e) {
          console.log(e);
          style.unuse();
          $(".sto-" + placeholder + "-container").remove();
          tracker.error({
            // "te": "onBuild-format",
            "tl": e
          });
        }

      }
      // </editor-fold> *********************************************************************


      // <editor-fold> CARROUSEL ARROWS *****************************************************
      if ($('.sto-' + placeholder + '-slider').width() <= $('.sto-' + placeholder + '-w-slider').width()) {
        $('.sto-' + placeholder + '-slider-right').fadeOut(1);
        $('.sto-' + placeholder + '-slider').css({
          'left': 'calc(50% + 8px)',
          'transform': 'translateX(-50%)'
        })
      }

      var slideDist = 400;
      $('.sto-' + placeholder + '-slider-left').fadeOut(1);


      // left
      $('.sto-' + placeholder + '-slider-left').on('click', function () {
        $('.sto-' + placeholder + '-slider-right').fadeIn(300);
        var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
        var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
        if ((positionInit * -1) < slideDist && positionInit < 50) {
          $('.sto-' + placeholder + '-slider.sto-product-container').animate({
            left: '50'
          }, 300, 'swing');
          $('.sto-' + placeholder + '-slider-left').fadeOut(300);
        } else if (positionInit < 50) {
          var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left + slideDist;
          $('.sto-' + placeholder + '-slider.sto-product-container').animate({
            left: positionLeft + 'px'
          }, 300, 'swing');
        }
        tracker.browse({
          "tl": "swipeLeft",
          "po": pos
        });
      });

      // right
      $('.sto-' + placeholder + '-slider-right').on('click', function () {
        $('.sto-' + placeholder + '-slider-left').fadeIn(300);
        var sliderContainerWidth = $('.sto-' + placeholder + '-w-slider').width();
        var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
        var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
        var positionRight = slideWidth - (sliderContainerWidth - positionInit);
        if (positionRight > slideDist) {
          var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left - slideDist;
          $('.sto-' + placeholder + '-slider.sto-product-container').animate({
            left: positionLeft + 'px'
          }, 300, 'swing');
        } else if (positionRight <= slideDist) {
          $('.sto-' + placeholder + '-slider.sto-product-container').animate({
            left: (sliderContainerWidth - slideWidth - 35) + 'px'
          }, 300, 'swing');
          $('.sto-' + placeholder + '-slider-right').fadeOut(300);
        }
        tracker.browse({
          "tl": "swipeRight",
          "po": pos
        });
      });


      // tracking swipe
      var lastPos = 0;
      $('.sto-' + placeholder + '-w-slider').on('scroll', function () {
        var currPos = $('.sto-' + placeholder + '-w-slider').scrollLeft();
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function () {

          if (currPos > lastPos) {
            tracker.browse({
              "tl": "swipeRight",
              "po": pos
            });
          }
          if (currPos < lastPos) {
            tracker.browse({
              "tl": "swipeLeft",
              "po": pos
            });
          }
          lastPos = currPos;
        }, 250));
      });

      // </editor-fold> *********************************************************************


      var heightFormat = function() {

          var products = $(".sto-product-container>li.product");
          var sliderContainer = $('.sto-BA_slider_-w-slider');
          var heightFormat = 0;
          products.each(function() {
              var thisHeight = $(this).height();
              if (thisHeight > heightFormat) {
                  heightFormat = thisHeight;
              }
          });
          sliderContainer.css("height", heightFormat + 50);
          products.css("height", heightFormat + 30);
      }
      //
      // setTimeout(function() {
      //     heightFormat();
      // }, 500);




      return removers;
    });

  }


} catch (e) {
  console.log(e);
}
