"use strict";

try {

  const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 2,
    ctxt_bg = 4,
    ctxt_logo = 5;

  var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    style = require("./main.css"),
    container = $(html),
    creaid = settings.creaid,
    format = settings.format,
    custom = settings.custom,
    container = $(html),
    helper_methods = sto.utils.retailerMethod,
    pos;


  var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);


  module.exports = {
    init: _init_()
  }

  function _init_() {

    sto.load([format, creaid], function(tracker) {

      var removers = {};
      removers[format] = {
        "creaid": creaid,
        "func": function() {
          style.unuse();
          container.remove(); //verifier containerFormat j'ai fait un copier coller
        }
      };


      var int, isElementInViewport;

      isElementInViewport = function(el) {

        if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
          el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          !(rect.top == rect.bottom || rect.left == rect.right) &&
          !(rect.height == 0 || rect.width == 0) &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
          rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );

      }

      /*if ($('div[data-pos="' + settings.btfpos + '"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }*/

      if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }


      // <editor-fold> CARROUSEL PRODUITS ***************************************************
      if (settings.crawl != "" && settings.products.length > 0 && settings.custom.length < 1) {

        helper_methods.crawl(settings.crawl).promise.then(function(d) {

          var prodCounter = 0;
          for (var f = 0; f < settings.products.length; f++) {
            if (d[settings.products[f]]) {
              prodCounter++;
            }
          }

          // if (prodCounter < 3) {
          //   tracker.error({
          //     "tl": "lessThanThreeProd"
          //   });
          //   return removers;
          //   return false;
          // }



          try {



            // <editor-fold> INSERT CONTAINER *****************************************************
            // if ($('div[data-pos="13"]').length <= 0 && $('div[data-pos="25"]').length <= 0) {
            //   container.attr('data-pos', '13');
            // } else if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length <= 0) {
            //   container.attr('data-pos', '25');
            // }

            var prodsInpage = $('#productList>li[data-productid]').length;

            if (prodsInpage >= 14 && prodsInpage <= 26 && $('.sto-' + format).length < 1) {
              helper_methods.createFormat(container, tracker, settings.products, d);
            } else if (prodsInpage > 26 && $('.sto-' + format).length < 3) {
              helper_methods.createFormat(container, tracker, settings.products, d);
            } else {
              tracker.error({
                "tl": "notEnoughProdsInPage"
              });
              return removers;
              return false;
            }




            var insertFormat = function() {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            var sponso = $('.hl-beacon-universal').length;
            if (sponso >= 1) {
              sponso = 1;
            }

            if ($('.sto-format').length == 0) {
              $('#productList>li').eq( nbTile * (5 + sponso)).before(container);
              $('.sto-format:first-of-type').attr('data-pos', 6 + sponso);
            } else if ($('.sto-format').length == 1) {
              $('#productList>li').eq( nbTile * (15 + sponso)).before(container);
              $('.sto-format:nth-of-type(2)').attr('data-pos', 16 + sponso);
            } else if ($('.sto-format').length == 2) {
              $('#productList>li').eq( nbTile * (25 + sponso)).before(container);
              $('.sto-format:nth-of-type(3)').attr('data-pos', 26 + sponso);
            }
          }

          insertFormat();



            // </editor-fold> *********************************************************************


            // console.log('hello');

            style.use();


            //$('#productList>li.product').eq(parseInt(settings.btfpos) - 1).before(container);


            tracker.display({
              "po": pos
            });

            //$('.sto-' + placeholder + '-w-carrousel').attr('data-pos', settings.btfpos);
            var lastProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').eq($('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length - 1).detach();

            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').prepend(lastProd);

            $('.sto-' + placeholder + '-w-carrousel').attr('data-type', 'products');
            $('.sto-' + placeholder + '-w-carrousel').attr('data-format-type', settings.format);
            $('.sto-' + placeholder + '-w-carrousel').attr('data-crea-id', settings.creaid);




            // <editor-fold> OLD SWIPE ************************************************************
            // function myfunction(el, d) {
            //   //var swipePx = x2 - x1;
            //   //console.log(swipePx);
            //   //alert("you swiped on element with id '" + el + "' to " + d + " direction");
            //   var pageW = $('#productList').width() - 15;
            //   var sliderW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').outerWidth();
            //   var prodW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').outerWidth();
            //   var sliderLeftPos = parseFloat($('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('left'));
            //   switch (d) {
            //
            //     case "l":
            //       tracker.browse({
            //         "tl": "swipeLeft",
            //         "po": pos
            //       })
                  /*if (sliderLeftPos == 0) {
                      $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                      'left': sliderLeftPos - (prodW / 2)
                    }, 300, function() {});
                  } else if (sliderLeftPos <= -(sliderW - pageW - prodW)) {
                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                      'left': -(sliderW - pageW)
                    }, 300, function() {});
                  } else {
                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                      'left': sliderLeftPos - prodW
                    }, 300, function() {});
                  }*/

                //   $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                //     'left': sliderLeftPos - prodW
                //   }, 300, function() {
                //     var movedProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').eq(0).detach();
                //     $('.sto-' + placeholder + '-w-carrousel .sto-product-container').append(movedProd);
                //
                //     sliderLeftPos = parseFloat($('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('left'));
                //     $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                //       'left': sliderLeftPos + prodW
                //     }, 0);
                //   });
                //
                //   break;
                //
                // case "r":
                //   tracker.browse({
                //     "tl": "swipeRight",
                //     "po": pos
                //   })
                  /*if (sliderLeftPos <= -(sliderW - pageW)) {
                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                      'left': sliderLeftPos + (prodW / 2)
                    }, 300, function() {});
                  } else if (sliderLeftPos >= -(prodW / 2) - 5) {
                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                      'left': 0
                    }, 300, function() {});
                  } else {
                    $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
                      'left': sliderLeftPos + prodW
                    }, 300, function() {});
                  }*/

            //       var movedProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').eq($('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length - 1).detach();
            //       $('.sto-' + placeholder + '-w-carrousel .sto-product-container').prepend(movedProd);
            //       $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
            //         'left': sliderLeftPos - prodW
            //       }, 0, function() {
            //         sliderLeftPos = parseFloat($('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('left'));
            //         $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
            //           'left': sliderLeftPos + prodW
            //         }, 300, function() {});
            //       });
            //
            //       break;
            //     default:
            //
            //   }
            //
            // }



            //detectswipe('sto-' + placeholder + '-slider', myfunction);


            // var swipeInRealTime = function() {
            //
            //   var elSlider = document.getElementById('sto-' + placeholder + '-slider'),
            //     sIni = 0,
            //     sMov = 0,
            //     sliderPos = 0,
            //     sliderPosMov = 0,
            //     finalMov = 0,
            //     trackSwipe = true,
            //     t = 0;
            //
            //   var slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length;
            //   var pageW = $('#productList').width() - 15;
            //   var sliderW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').outerWidth();
            //   var prodW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').eq(0).outerWidth(true, true);
            //
            //   elSlider.addEventListener('touchstart', function(e) {
            //     t = e.touches[0];
            //     sIni = t.screenX;
            //     sliderPos = parseFloat($('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('left'));
            //     sliderW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').outerWidth();
            //     setTimeout(function() {
            //       trackSwipe = true;
            //     }, 600);
            //   }, false);
            //
            //   elSlider.addEventListener('touchmove', function(e) {
            //     e.preventDefault();
            //     t = e.touches[0];
            //     sMov = t.screenX;
            //     finalMov = Math.abs(sIni - sMov);
            //
            //     //ACTION
            //     if (sMov > sIni) { //right
            //       if (trackSwipe == true) {
            //         tracker.browse({
            //           "tl": "swipeRight",
            //           "po": pos
            //         });
            //       }
            //       if (sliderPos < 0 && sliderPosMov < 0) {
            //         $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css({
            //           'left': Math.ceil(sliderPos + finalMov)
            //         });
            //       } else if (sliderPos >= 0) {
            //         $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css({
            //           'left': 0
            //         });
            //       }
            //     } else { //left
            //       if (trackSwipe == true) {
            //         tracker.browse({
            //           "tl": "swipeLeft",
            //           "po": pos
            //         })
            //       }
            //       if (sliderPosMov <= -Math.abs(sliderW - pageW)) {
            //         $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css({
            //           'left': -(sliderW - pageW)
            //         }); ////
            //       } else {
            //         $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css({
            //           'left': Math.ceil(sliderPos - finalMov)
            //         });
            //       }
            //     }
            //     trackSwipe = false;
            //     sliderPosMov = parseFloat($('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('left'));
            //   }, false);
            // };
            // swipeInRealTime();


            //$('.sto-' + placeholder + '-w-carrousel').attr('data-type', settings.type);
            // </editor-fold> *********************************************************************



            var slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length,
              pageW, slidesQty, sliderW;



            window.addEventListener("resize", function() {

              var resizeFormat = function() {
                var widthTile = $('#productList>li').width();
                var widthLine = $('.product-list').width();
                var nbTile;

                if (widthTile * 2 > widthLine) {
                  nbTile = 1;
                } else if (widthTile * 3 > widthLine) {
                  nbTile = 2;
                } else {
                  nbTile = 3;
                }

                $('.sto-format').each(function() {
                  var dataPos = $(this).attr('data-pos');

                  if (window.matchMedia("(max-width: 767px)").matches) {
                    $('#productList>li:visible').eq( nbTile * (dataPos - 1)).before(this);
                  } else {
                    $('#productList>li').eq( nbTile * (dataPos - 1)).before(this);
                  }

                });
              }
              resizeFormat();

              var size = $(window).width();

              pageW = $('#productList').outerWidth() - 15;
              slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length;
              $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').width((pageW / 2) - 14);
              sliderW = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').outerWidth();
              $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width(sliderW * slidesQty);


              //Format height homotetique
              if (size < 400) {
                $('.sto-' + placeholder + '-w-carrousel').css('height', ((90 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
              } else {
                $('.sto-' + placeholder + '-w-carrousel').css('height', ((80 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
              }

              $('.sto-' + placeholder + '-w-carrousel .pl-product').css('width', ((40 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
              $('.sto-' + placeholder + '-w-carrousel .pl-image').css('height', $('.sto-' + placeholder + '-product-img').width());



              $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('width', $('.sto-' + placeholder + '-w-carrousel .pl-product').outerWidth( true ) * slidesQty + 10 + 'px');

              //Logo height homotetique
              $('.sto-' + placeholder + '-logo').css('height', ((25 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');


              // slider left margin
              if (window.matchMedia("(max-width: 767px)").matches) {
                $('.sto-' + placeholder + '-slider.sto-product-container').css('left', '15px');
              } else {
                $('.sto-' + placeholder + '-slider.sto-product-container').css('left', '50px');
              }

            });


              // <editor-fold> CARROUSEL ARROWS *****************************************************
                  var slideDist = 400;

                  // left
                  $('.sto-' + placeholder + '-slider-left').on('click', function() {
                    var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
                    var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
                    if ((positionInit * -1) < slideDist && positionInit < 50) {
                      $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: '50'}, 300, 'swing');
                    } else if (positionInit < 50) {
                      var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left + slideDist;
                      $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: positionLeft + 'px'}, 300, 'swing');
                    }
                    tracker.browse({
                        "tl": "swipeLeft",
                      });
                  });

                  // right
                  $('.sto-' + placeholder + '-slider-right').on('click', function() {
                    var sliderContainerWidth = $('.sto-' + placeholder + '-w-slider').width();
                    var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
                    var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
                    var positionRight = slideWidth - (sliderContainerWidth - positionInit);
                    if (positionRight > slideDist) {
                      var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left - slideDist;
                      $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: positionLeft + 'px'}, 300, 'swing');
                    } else if (positionRight <= slideDist ) {
                      $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: (sliderContainerWidth - slideWidth - 35) + 'px'}, 300, 'swing');
                    }
                    tracker.browse({
                        "tl": "swipeRight",
                      });
                  })

                  // tracking swipe
                  var lastPos = 0;
                    $('.sto-' + placeholder + '-w-slider').on('scroll', function() {
                      var currPos = $('.sto-' + placeholder + '-w-slider').scrollLeft();
                          clearTimeout($.data(this, 'scrollTimer'));
                          $.data(this, 'scrollTimer', setTimeout(function() {

                            if (currPos > lastPos) {
                                console.log('scroll right');
                                tracker.browse({
                                    "tl": "swipeRight",
                                  });
                            }
                            if (currPos < lastPos)
                            {
                                console.log('scroll left');
                                tracker.browse({
                                    "tl": "swipeLeft",
                                  });
                            }
                            lastPos = currPos;
                          }, 250));
                      });

                  // </editor-fold> *********************************************************************


            window.setTimeout(function() {
              //window.dispatchEvent(new window.Event("resize"));
              var evt = window.document.createEvent('UIEvents');
              evt.initUIEvent('resize', true, false, window, 0);
              window.dispatchEvent(evt);
            });

            //STORE FORMAT FOR FILTERS
            sto.globals["print_creatives"].push({
              html: $('.sto-' + placeholder + '-w-carrousel'),
              parent_container_id: "#productList",
              type: settings.format,
              crea_id: settings.creaid,
              position: pos
            });

          } catch (e) {
            style.unuse();
            $(".sto-" + placeholder + "-container").remove();
            tracker.error({
              "te": "onBuild-format",
              "tl": e
            });
          }
        }).then(null, console.log.bind(console));

      }
      // </editor-fold> *********************************************************************

      else

      // <editor-fold> CARROUSEL PUB ********************************************************
      {
        if (settings.custom.length < 2) {
          tracker.error({
            "tl": "lessThanTwoProds"
          });
          return removers;
          return false;
        }

        try {


          var prodsInpage = $('#productList>li[data-productid]').length;


          if (prodsInpage >= 14 && prodsInpage <= 26 && $('.sto-' + format).length < 1) {

          } else if (prodsInpage > 26 && $('.sto-' + format).length < 3) {

          } else {
            tracker.error({
              "tl": "notEnoughProdsInPage"
            });
            return removers;
            return false;
          }

          style.use();



          /*  if ($('.sto-' + format).length < 1) {
              $('#productList>li[data-productid]').eq(12).before(container);
            } else if ($('.sto-' + format).length == 1) {
              $('#productList>li[data-productid]').eq(18).before(container);
            }*/

          // <editor-fold> INSERT CONTAINER *****************************************************
          var insertFormat = function() {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            if ($('.sto-format').length == 0) {
              $('#productList>li').eq( nbTile * 5 ).before(container);
              $('.sto-format:first-of-type').attr('data-pos', '6');
            } else if ($('.sto-format').length == 1) {
              $('#productList>li').eq( nbTile * 15 ).before(container);
              $('.sto-format:nth-of-type(2)').attr('data-pos', '16');
            } else if ($('.sto-format').length == 2) {
              $('#productList>li').eq( nbTile * 25 ).before(container);
              $('.sto-format:nth-of-type(3)').attr('data-pos', '26');
            }
          }

          insertFormat();
          // </editor-fold> *********************************************************************


          tracker.display({
            "po": pos
          });


          $('.sto-' + placeholder + '-w-carrousel').attr('data-type', 'redirection');
          // $('.sto-' + placeholder + '-w-carrousel').attr('data-pos', settings.btfpos);
          $('.sto-' + placeholder + '-w-carrousel').attr('data-format-type', settings.format);
          $('.sto-' + placeholder + '-w-carrousel').attr('data-crea-id', settings.creaid);



          //VIEW TRACKING
          int = window.setInterval(function() {
            if (isElementInViewport(container)) {
              tracker.view({
                "po": pos
              });
              window.clearInterval(int);
            }
          }, 200);


          // <editor-fold> INSERT PRODUITS ******************************************************
          for (var i = 0; i < settings.custom.length; i++) {
            var prodInd = parseInt(settings.custom[i][0]);
            var prodClaimTxt = settings.custom[i][1];
            var prodCtaUrl = settings.custom[i][2];
            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').append('<li data-index="' + (prodInd + 1) + '" class="product"><div class="sto-' + placeholder + '-product-img"></div><div class="sto-' + placeholder + '-w-cta"><div class="sto-' + placeholder + '-product-claim"><span>' + prodClaimTxt + '</span></div><div data-href="' + prodCtaUrl + '" class="sto-' + placeholder + '-product-cta"><span>' + settings.cta_text + '</span></div></div></li>');
          }

          // largeur carousel resize
          var carWidth = function() {

            var nbProduits = settings.custom.length;
            // console.log(nbProduits);
            var widthProd = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').width();
            // console.log(widthProd);
          }

          carWidth();

          // </editor-fold> *********************************************************************


          // <editor-fold> CLICK REDIRECTION ****************************************************
          $('.sto-' + placeholder + '-w-slider .sto-product-container .product').on('click', function() {
            tracker.click({
              "tl": 'prod' + (parseInt($(this).attr('data-index') - 1)) + '_' + $(this).find('.sto-' + placeholder + '-product-claim span').text().replace(/ /g, ''),
              "po": pos
            });
            window.open($(this).find('.sto-' + placeholder + '-product-cta').attr('data-href'), '_self');
          });
          // </editor-fold> *********************************************************************







          var slidesQty = $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').length,
            pageW, slidesQty, sliderW;
          window.addEventListener("resize", function() {

            var resizeFormat = function() {
              var widthTile = $('#productList>li').width();
              var widthLine = $('.product-list').width();
              var nbTile;

              if (widthTile * 2 > widthLine) {
                nbTile = 1;
              } else if (widthTile * 3 > widthLine) {
                nbTile = 2;
              } else {
                nbTile = 3;
              }

              $('.sto-format').each(function() {
                var dataPos = $(this).attr('data-pos');
                $('#productList>li').eq( nbTile * (dataPos - 1)).before(this);
              });
            }
            resizeFormat();


            sliderW = 0;
            for (var j = 0; j < slidesQty; j++) {
              sliderW = sliderW + $('.sto-' + placeholder + '-w-carrousel .sto-product-container>li').eq(j).outerWidth(true, true);
            }

            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').outerWidth(sliderW + 2);

            //Format height homotetique
            $('.sto-' + placeholder + '-w-carrousel').css('height', ((65 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');

            $('.sto-' + placeholder + '-w-carrousel .product').css('width', ((40 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
            $('.sto-' + placeholder + '-product-img').css('height', $('.sto-' + placeholder + '-product-img').width());
            //Logo height homotetique
            $('.sto-' + placeholder + '-logo').css('width', ((33 * $('.sto-' + placeholder + '-w-carrousel').width()) / 100) + 'px');
            // $('.sto-' + placeholder + '-logo').css('height', ((75 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');

            $('.sto-' + placeholder + '-w-carrousel .sto-product-container').css('width', $('.sto-' + placeholder + '-w-carrousel .product').outerWidth( true ) * slidesQty + 'px');


            // displostion fleches
            // var heightImg = $('.sto-' + placeholder + '-product-img').css('height');
            // $('.sto-' + placeholder + '-slider-left, .sto-' + placeholder + '-slider-right').css('height', heightImg);


            // slider left margin
            if (window.matchMedia("(max-width: 767px)").matches) {
              $('.sto-' + placeholder + '-slider.sto-product-container').css('left', '15px');
            } else {
              $('.sto-' + placeholder + '-slider.sto-product-container').css('left', '50px');
            }

            // $('.sto-' + placeholder + '-w-carrousel .sto-product-container').animate({
            //   'left': 15
            // }, 0);

          });
          window.setTimeout(function() {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
          });

          //STORE FORMAT FOR FILTERS
          // sto.globals["print_creatives"].push({
          //   html: $('.sto-' + placeholder + '-w-carrousel'),
          //   parent_container_id: "#productList",
          //   type: settings.format,
          //   crea_id: settings.creaid,
          //   position: pos
          // });

        } catch (e) {
          console.log(e);
          style.unuse();
          $(".sto-" + placeholder + "-container").remove();
          tracker.error({
            "te": "onBuild-format",
            "tl": e
          });
        }

      }
      // </editor-fold> *********************************************************************


      // <editor-fold> CARROUSEL ARROWS *****************************************************
      var slideDist = 400;

      // if (window.matchMedia("(min-width: 768px)").matches) {
        // left
        $('.sto-' + placeholder + '-slider-left').on('click', function() {
          var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
          var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
          if ((positionInit * -1) < slideDist && positionInit < 50) {
            $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: '50'}, 300, 'swing');
          } else if (positionInit < 50) {
            var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left + slideDist;
            $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: positionLeft + 'px'}, 300, 'swing');
          }
          tracker.browse({
              "tl": "swipeLeft",
            });
        });

        // right
        $('.sto-' + placeholder + '-slider-right').on('click', function() {
          var sliderContainerWidth = $('.sto-' + placeholder + '-w-slider').width();
          var slideWidth = $('.sto-' + placeholder + '-w-carrousel .sto-product-container').width();
          var positionInit = $('.sto-' + placeholder + '-slider.sto-product-container').position().left;
          var positionRight = slideWidth - (sliderContainerWidth - positionInit);
          if (positionRight > slideDist) {
            var positionLeft = $('.sto-' + placeholder + '-slider.sto-product-container').position().left - slideDist;
            $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: positionLeft + 'px'}, 300, 'swing');
          } else if (positionRight <= slideDist ) {
            $('.sto-' + placeholder + '-slider.sto-product-container').animate({left: (sliderContainerWidth - slideWidth - 35) + 'px'}, 300, 'swing');
          }
          tracker.browse({
              "tl": "swipeRight",
            });
        });
      // }

      // tracking swipe
      var lastPos = 0;
        $('.sto-' + placeholder + '-w-slider').on('scroll', function() {
          var currPos = $('.sto-' + placeholder + '-w-slider').scrollLeft();
              clearTimeout($.data(this, 'scrollTimer'));
              $.data(this, 'scrollTimer', setTimeout(function() {

                if (currPos > lastPos) {
                    console.log('scroll right');
                    tracker.browse({
                        "tl": "swipeRight",
                      });
                }
                if (currPos < lastPos)
                {
                    console.log('scroll left');
                    tracker.browse({
                        "tl": "swipeLeft",
                      });
                }
                lastPos = currPos;
              }, 250));
          });

      // </editor-fold> *********************************************************************


      return removers;
    });

  }


} catch (e) {
  console.log(e);
}
