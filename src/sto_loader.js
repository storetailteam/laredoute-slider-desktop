var settings = require("./settings.json");
settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/"+settings.logo_img+")";
settings.claim_text_color = settings.claim_text_color === "" ? "#000" : settings.claim_text_color;
settings.cta_border_color = settings.cta_border_color === "" ? "#000" : settings.cta_border_color;
settings.cta_bg_color = settings.cta_bg_color === "" ? "#fff" : settings.cta_bg_color;
settings.cta_text_color = settings.cta_text_color === "" ? "#000" : settings.cta_text_color;
settings.prod_img_01 = settings.prod_img_01 === "" ? "none" : "url(./../../img/"+settings.prod_img_01+")";
settings.prod_img_02 = settings.prod_img_02 === "" ? "none" : "url(./../../img/"+settings.prod_img_02+")";
settings.prod_img_03 = settings.prod_img_03 === "" ? "none" : "url(./../../img/"+settings.prod_img_03+")";
settings.prod_img_04 = settings.prod_img_04 === "" ? "none" : "url(./../../img/"+settings.prod_img_04+")";
settings.prod_img_05 = settings.prod_img_05 === "" ? "none" : "url(./../../img/"+settings.prod_img_05+")";
module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, `${settings.format}_${settings.name}_${settings.creaid}`)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__LOGO_IMG__/g,settings.logo_img)
        .replace(/__CLAIM_TXT_COLOR__/g,settings.claim_text_color)
        .replace(/__CTA_BORDER_COLOR__/g,settings.cta_border_color)
        .replace(/__CTA_BG_COLOR__/g,settings.cta_bg_color)
        .replace(/__CTA_TXT_COLOR__/g,settings.cta_text_color)
        .replace(/__PROD_01__/g,settings.prod_img_01)
        .replace(/__PROD_02__/g,settings.prod_img_02)
        .replace(/__PROD_03__/g,settings.prod_img_03)
        .replace(/__PROD_04__/g,settings.prod_img_04)
        .replace(/__PROD_05__/g,settings.prod_img_05);
    return test;
};
